# 目录
- controller.gas
  - 燃气订单管理
    - [查询订单信息列表](#查询订单信息列表)
    - [查询订单操作流水/交易流水](#查询订单操作流水/交易流水)
    - [查看订单详情](#查看订单详情)
    - [新建订单](#新建订单)
    - [派单](#派单)
    - [接单](#接单)
    - [订单完成](#订单完成)
    - [确认收款](#确认收款)
    - [退单](#退单)
    - [退单审核](#退单审核)
    - [微信公众号支付](#微信公众号支付)
- controller.goods
  - 商品管理
    - [新增商品](#新增商品)
    - [删除商品](#删除商品)
    - [修改商品，只修改商品基础信息](#修改商品，只修改商品基础信息)
    - [修改商品销售状态，待售，销售中，下架](#修改商品销售状态，待售，销售中，下架)
    - [修改商品库存](#修改商品库存)
    - [修改商品标题轮播图片](#修改商品标题轮播图片)
    - [修改商品关联企业门店](#修改商品关联企业门店)
    - [无权限获取商品详细信息](#无权限获取商品详细信息)
    - [无权限获取商品列表](#无权限获取商品列表)
    - [获取所有商品信息](#获取所有商品信息)
  - 商品费用管理
    - [新增商品费用](#新增商品费用)
    - [删除商品费用](#删除商品费用)
    - [修改商品费用信息](#修改商品费用信息)
    - [修改商品费用是否启用](#修改商品费用是否启用)
    - [无权限根据商品id获取商品费用](#无权限根据商品id获取商品费用)
    - [根据商品id获取商品费用](#根据商品id获取商品费用)
  - 商品操作记录管理
    - [获取商品操作记录列表](#获取商品操作记录列表)
  - 商品分类管理
    - [新增商品分类](#新增商品分类)
    - [删除商品分类](#删除商品分类)
    - [修改商品分类，包括修改关联企业](#修改商品分类，包括修改关联企业)
    - [根据id获取商品分类信息](#根据id获取商品分类信息)
    - [获取商品分类列表](#获取商品分类列表)
    - [公开获取商品分类列表](#公开获取商品分类列表)
- controller.wechat
  - 微信支付回调
    - [付款回调](#付款回调)

 [返回目录](#目录)



- 字段含义

| orderid                 | 订单id                        |
| ----------------------- | ----------------------------- |
| orderman                | 下单人                        |
| ordermanid              | 下单人标识                    |
| ordertime               | 下单时间                      |
| ordertel                | 下单人联系电话                |
| accountno               | 户号                          |
| uname                   | 用户名称                      |
| uinfoid                 | 用户标识                      |
| utype                   | 用户类型                      |
| dutype                  | 用气类型                      |
| address                 | 地址                          |
| responsibilityman       | 负责人                        |
| responsibilitymantel    | 负责人电话                    |
| areacode                | 所属区域                      |
| areacodename            | 所属区域名称                  |
| supplyent               | 供气企业                      |
| supplyentid             | 供气企业标识                  |
| supplystation           | 所属门店                      |
| supplystationid         | 所属门店标识                  |
| orderprice              | 订单总价                      |
| discountprice           | 优惠金额                      |
| shouldprice             | 应收金额                      |
| generateintegralnum     | 生成积分                      |
| buychannel              | 订购渠道                      |
| useintegralnum          | 使用积分数                    |
| paymode                 | 支付方式                      |
| paystate                | 支付状态                      |
| paytime                 | 支付时间                      |
| transactionflowid       | 交易流水号                    |
| orderstate              | 订单状态                      |
| assignman               | 派单人                        |
| assignmanid             | 派单人标识                    |
| assigntime              | 派单时间                      |
| assignduration          | 派单时长                      |
| appointmentarrivetime   | 预约送达时间                  |
| workman                 | 送气工名称                    |
| workmanaccount          | 送气工账号标识                |
| workmanid               | 送气工标识                    |
| workmanreceivetime      | 接单时间                      |
| workmanreceiveduration  | 接单时长                      |
| arrivetime              | 送达时间                      |
| arrivelocation          | 送达位置定位 格式 : 纬度,经度 |
| arriveduration          | 配送时长                      |
| signedbyimg             | 签收人确认图片                |
| signedstate             | 签收状态                      |
| signedtime              | 签收时间                      |
| moneyconfirmstate       | 收款确认状态                  |
| moneyconfirmedby        | 收款确认审核人                |
| moneyconfirmedbyid      | 收款确认审核人标识            |
| moneyconfirmtime        | 收款确认审核时间              |
| ordercompleteduration   | 订单完成总时长                |
| chargebackstate         | 退单状态                      |
| chargebacktime          | 退单时间                      |
| chargebackconfirmedby   | 退单审核人                    |
| chargebackconfirmedbyid | 退单审核人标识                |
| chargebackconfirmtime   | 退单审核时间                  |
| chargebackconfirmremark | 退单审核备注                  |
| chargebackpayprice      | 退单退款金额                  |
| chargebackreason        | 退单原因                      |
| remindman               | 催单人                        |
| remindmanid             | 催单人标识                    |
| remindtime              | 催单时间                      |
| reassignmentman         | 转派人                        |
| reassignmentmanid       | 转派人标识                    |
| reassignmenttime        | 转派时间                      |
| remark                  | 备注                          |
| createman               | 订单创建人                    |
| createmanid             | 订单创建人标识                |
| createtime              | 订单创建时间                  |
| credentialimg           | 凭据照片                      |
| wechatouttradeno        | 微信支付关联单号              |
| ulocation               | 用户定位 格式 : 纬度,经度     |
| completedtime           | 送达时间     |



# 查询订单信息列表

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/list?pageNum=739&pageSize=243&orderid=订单id22&ordermanid=下单人标识126&ordertimeStart=下单时间开始74&ordertimeEnd=下单时间结束121&accountno=户号94&uinfoid=用户标识107&utype=用户类型37&dutype=用气类型115&areacode=所属区域119&supplyentid=供气企业标识126&supplystationid=所属门店标识122&buychannel=订购渠道1&paymode=支付方式69&paystate=支付状态124&orderstate=订单状态49&assignmanid=派单人标识74&workmanaccount=送气工账号标识113&workmanid=送气工标识27&signedstate=签收状态13&moneyconfirmstate=收款确认状态33&chargebackstate=退单状态105&showgoods=是否显示商品信息93
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
//pageNum:739
//pageSize:243
//orderid:订单id22
//ordermanid:下单人标识126
//ordertimeStart:下单时间开始74
//ordertimeEnd:下单时间结束121
//accountno:户号94
//uinfoid:用户标识107
//utype:用户类型37
//dutype:用气类型115
//areacode:所属区域119
//supplyentid:供气企业标识126
//supplystationid:所属门店标识122
//buychannel:订购渠道1
//paymode:支付方式69
//paystate:支付状态124
//orderstate:订单状态49
//assignmanid:派单人标识74
//workmanaccount:送气工账号标识113
//workmanid:送气工标识27
//signedstate:签收状态13
//moneyconfirmstate:收款确认状态33
//chargebackstate:退单状态105
//showgoods:是否显示商品信息93

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNum     | **Long**     | 否  |   | 最小值为 1  |
| pageSize     | **Long**     | 否  |   | 最小值为 1  |
| orderid     | **String**     | 否  |  订单id |   |
| ordermanid     | **String**     | 否  |  下单人标识 |   |
| ordertimeStart     | **String**     | 否  |  下单时间开始 |   |
| ordertimeEnd     | **String**     | 否  |  下单时间结束 |   |
| accountno     | **String**     | 否  |  户号 |   |
| uinfoid     | **String**     | 否  |  用户标识 |   |
| utype     | **String**     | 否  |  用户类型 |   |
| dutype     | **String**     | 否  |  用气类型 |   |
| areacode     | **String**     | 否  |  所属区域 |   |
| supplyentid     | **String**     | 否  |  供气企业标识 |   |
| supplystationid     | **String**     | 否  |  所属门店标识 |   |
| buychannel     | **String**     | 否  |  订购渠道 |   |
| paymode     | **String**     | 否  |  支付方式 |   |
| paystate     | **String**     | 否  |  支付状态 |   |
| orderstate     | **String**     | 否  |  订单状态 |   |
| assignmanid     | **String**     | 否  |  派单人标识 |   |
| workmanaccount     | **String**     | 否  |  送气工账号标识 |   |
| workmanid     | **String**     | 否  |  送气工标识 |   |
| signedstate     | **String**     | 否  |  签收状态 |   |
| moneyconfirmstate     | **String**     | 否  |  收款确认状态 |   |
| chargebackstate     | **String**     | 否  |  退单状态 |   |
| showgoods     | **String**     | 否  | 是否显示商品信息, true false |   |

## 出参
### 出参示例
```json
{
    "code": "0",
    "data": {
        "total": 1,
        "list": [
            {
                "orderid": "a0698ee656c14dc694884ad138e3b307",
                "orderman": "秦小博",
                "ordermanid": "ae8af77a2efc4329a7ba440bcef699b1",
                "ordertime": "2023-04-06 07:42:26",
                "ordertel": "123456789098765",
                "accountno": "TEST00000001",
                "uname": "测试用户名称",
                "uinfoid": "de6d4263ac3e4ac5af8e0ba18c0ecef7",
                "utype": "restaurant",
                "dutype": "yhq",
                "address": "测试地址",
                "responsibilityman": "测试负责人",
                "responsibilitymantel": "18552026779",
                "areacode": "320206001",
                "areacodename": "江苏省无锡市惠山区堰桥街道",
                "supplyent": "供气企业001",
                "supplyentid": "9d14f4c79abb48a0a30fd82040c54640",
                "supplystation": "供应站001",
                "supplystationid": "9d14f4c79abb48a0a30fd82040c59012",
                "orderprice": 0,
                "discountprice": 0,
                "shouldprice": 0,
                "generateintegralnum": 0,
                "buychannel": "phone",
                "useintegralnum": 0,
                "paymode": "cash",
                "paystate": "unPaid",
                "paytime": null,
                "transactionflowid": "86044101f71445c4ac64b5b0161578e4",
                "orderstate": "toBeDistributed",
                "assignman": null,
                "assignmanid": null,
                "assigntime": null,
                "assignduration": null,
                "appointmentarrivetime": "2023-04-04 18:00",
                "workman": null,
                "workmanaccount": null,
                "workmanid": null,
                "workmanreceivetime": null,
                "workmanreceiveduration": null,
                "arrivetime": null,
                "arrivelocation": null,
                "arriveduration": null,
                "signedbyimg": null,
                "signedstate": null,
                "signedtime": null,
                "moneyconfirmstate": null,
                "moneyconfirmedby": null,
                "moneyconfirmedbyid": null,
                "moneyconfirmtime": null,
                "ordercompleteduration": null,
                "chargebackstate": null,
                "chargebacktime": null,
                "chargebackconfirmedby": null,
                "chargebackconfirmedbyid": null,
                "chargebackconfirmtime": null,
                "chargebackconfirmremark": null,
                "chargebackpayprice": null,
                "chargebackreason": null,
                "remindman": null,
                "remindmanid": null,
                "remindtime": null,
                "reassignmentman": null,
                "reassignmentmanid": null,
                "reassignmenttime": null,
                "remark": "备注001",
                "createman": "user_zMNImU",
                "createmanid": "640a3a775c2e4b6bb6d9a7cfe6218a40",
                "createtime": "2023-04-06 07:42:26",
                "credentialimg": null,
                "entGasorderGoods": [ // 商品信息
                    {
                        "id": "974598838f7746b287311699c6325de9",
                        "orderid": "a0698ee656c14dc694884ad138e3b307",
                        "goodid": "7985c6185c3f4b0687e61e3097fccc50",
                        "goodimg": "/a/b/c.jpg",
                        "goodname": "商品图片下面的那个又大又黑的主标题1",
                        "goodtype": "液化气",
                        "goodmodel": "商品型号4",
                        "goodunitprice": 0,
                        "goodnum": 2,
                        "gooddesc": "主标题下面的那个灰灰的小小的副标题2",
                        "goodsdetail": "{\"goodFy\":[{\"createTime\":\"2023-03-31 15:36:51\",\"creater\":\"test@001租户管理员\",\"ed\":\"2023-05-01\",\"goodsId\":\"7985c6185c3f4b0687e61e3097fccc50\",\"id\":\"12e1beb75e5843d292f19ae729b502a4\",\"lastUpdateTime\":\"2023-03-31 17:21:43\",\"lastUpdater\":\"test@001租户管理员\",\"sfbm\":\"cs11\",\"sfdw\":\"瓶\",\"sffx\":0,\"sfje\":9900,\"sfmc\":\"瓶装液化石油气\",\"sfms\":\"一瓶液化气的钱啊\",\"sfqt\":[\"factory\",\"restaurant\"],\"sortIndex\":0,\"st\":\"2023-03-01\",\"state\":\"1\"},{\"createTime\":\"2023-04-03 11:02:37\",\"creater\":\"test@001租户管理员\",\"ed\":\"2099-01-01\",\"goodsId\":\"7985c6185c3f4b0687e61e3097fccc50\",\"id\":\"6c5506fb301b45a3ab485d1d889bd5bb\",\"sfbm\":\"sl01\",\"sfdw\":\"次\",\"sffx\":0,\"sfje\":1000,\"sfmc\":\"上楼费\",\"sfms\":\"上楼费用\",\"sfqt\":[\"factory\",\"restaurant\"],\"sortIndex\":11,\"st\":\"2023-01-01\",\"state\":\"1\"}],\"good\":{\"coverPic\":{\"basename\":\"picTest\",\"fileUrl\":\"/a/b/c.jpg\",\"filetype\":\"jpeg\",\"fsize\":\"1024\",\"id\":\"7c34419b82e3d0b730fd82040c5\",\"status\":\"0\"},\"coverPicId\":\"7c34419b82e3d0b730fd82040c5\",\"createTime\":\"2023-03-31 10:51:12\",\"creater\":\"test@001租户管理员\",\"details\":\"超详细的商品介绍，来自富文本编辑器3\",\"ents\":[{\"areacode\":\"320206001\",\"areaname\":\"中华人民共和国,江苏省,无锡市,惠山区,堰桥街道\",\"qid\":\"9d14f4c79abb48a0a30fd82040c54640\",\"qname\":\"测试企业\",\"ssts\":[null,{\"gid\":\"9d14f4c79abb48a0a30fd82040c59012\",\"gname\":\"测试供应站\"}]}],\"goodsType\":{\"createTime\":\"2023-03-31 10:44:56\",\"creater\":\"test@001租户管理员\",\"goodsType\":\"液化气\",\"goodsTypeRemark\":\"瓶装液化石油气\",\"id\":\"611ce99f084148e186cccc8c263f3b8c\",\"parentId\":\"0\"},\"id\":\"7985c6185c3f4b0687e61e3097fccc50\",\"lastUpdateTime\":\"2023-03-31 10:59:37\",\"lastUpdater\":\"test@001租户管理员\",\"maintitle\":\"商品图片下面的那个又大又黑的主标题1\",\"model\":\"商品型号4\",\"remark\":\"商品备注5\",\"saleTypes\":[\"telphone\",\"wechat\"],\"saletimeEnd\":\"09:01\",\"saletimeStart\":\"17:59\",\"sortIndex\":1,\"state\":\"IN_SALE\",\"stateText\":\"销售中\",\"stock\":99,\"subtitle\":\"主标题下面的那个灰灰的小小的副标题2\",\"titlePics\":[{\"basename\":\"picTest\",\"fileUrl\":\"/a/b/c.jpg\",\"filetype\":\"jpeg\",\"fsize\":\"1024\",\"id\":\"7c34419b82e3d0b730fd82040c5\",\"status\":\"0\"}],\"typeId\":\"611ce99f084148e186cccc8c263f3b8c\"}}",
                        "totalprice": 0,
                        "createtime": "2023-04-06 07:42:26"
                    }
                ]
            }
        ],
        "pageNum": 1,
        "pageSize": 10,
        "size": 1,
        "startRow": 1,
        "endRow": 1,
        "pages": 1,
        "prePage": 0,
        "nextPage": 0,
        "isFirstPage": true,
        "isLastPage": true,
        "hasPreviousPage": false,
        "hasNextPage": false,
        "navigatePages": 8,
        "navigatepageNums": [
            1
        ],
        "navigateFirstPage": 1,
        "navigateLastPage": 1
    },
    "desc": "成功"
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 查询订单操作流水/交易流水

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/flow?orderid=Ytglbrjur&transactionflowid=yePTlmWvreBckyVV
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
orderid:Ytglbrjur
//transactionflowid:yePTlmWvreBckyVV

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | **是**  |   |   |
| transactionflowid     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "code": "0",
  "data": [
    {
      "id": "2d979dc0ba9944d697c3fb53aa0fdb7f",
      "orderid": "a0698ee656c14dc694884ad138e3b307", 
      "transactionflowid": null,
      "operatetime": "2023-04-06 07:42:26", //操作时间
      "operateman": "user_zMNImU", //操作人名称
      "operatemanid": "640a3a775c2e4b6bb6d9a7cfe6218a40", //操作人id
      "operatetype": "create", //操作类型
      "operatecontent": "新建订单", //操作内容
      "linkedkey": null,
      "linkeddata": null
    }
  ],
  "desc": "成功"
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 查看订单详情

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/detail?orderid=DUgpxWp
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
orderid:DUgpxWp

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "code": "0",
  "data": {
    "orderid": "a0698ee656c14dc694884ad138e3b307",
    "orderman": "秦小博",
    "ordermanid": "ae8af77a2efc4329a7ba440bcef699b1",
    "ordertime": "2023-04-06 07:42:26",
    "ordertel": "123456789098765",
    "accountno": "TEST00000001",
    "uname": "测试用户名称",
    "uinfoid": "de6d4263ac3e4ac5af8e0ba18c0ecef7",
    "utype": "restaurant",
    "dutype": "yhq",
    "address": "测试地址",
    "responsibilityman": "测试负责人",
    "responsibilitymantel": "18552026779",
    "areacode": "320206001",
    "areacodename": "江苏省无锡市惠山区堰桥街道",
    "supplyent": "供气企业001",
    "supplyentid": "9d14f4c79abb48a0a30fd82040c54640",
    "supplystation": "供应站001",
    "supplystationid": "9d14f4c79abb48a0a30fd82040c59012",
    "orderprice": 0,
    "discountprice": 0,
    "shouldprice": 0,
    "generateintegralnum": 0,
    "buychannel": "phone",
    "useintegralnum": 0,
    "paymode": "cash",
    "paystate": "unPaid",
    "paytime": null,
    "transactionflowid": "86044101f71445c4ac64b5b0161578e4",
    "orderstate": "toBeDistributed",
    "assignman": null,
    "assignmanid": null,
    "assigntime": null,
    "assignduration": null,
    "appointmentarrivetime": "2023-04-04 18:00",
    "workman": null,
    "workmanaccount": null,
    "workmanid": null,
    "workmanreceivetime": null,
    "workmanreceiveduration": null,
    "arrivetime": null,
    "arrivelocation": null,
    "arriveduration": null,
    "signedbyimg": null,
    "signedstate": null,
    "signedtime": null,
    "moneyconfirmstate": null,
    "moneyconfirmedby": null,
    "moneyconfirmedbyid": null,
    "moneyconfirmtime": null,
    "ordercompleteduration": null,
    "chargebackstate": null,
    "chargebacktime": null,
    "chargebackconfirmedby": null,
    "chargebackconfirmedbyid": null,
    "chargebackconfirmtime": null,
    "chargebackconfirmremark": null,
    "chargebackpayprice": null,
    "chargebackreason": null,
    "remindman": null,
    "remindmanid": null,
    "remindtime": null,
    "reassignmentman": null,
    "reassignmentmanid": null,
    "reassignmenttime": null,
    "remark": "备注001",
    "createman": "user_zMNImU",
    "createmanid": "640a3a775c2e4b6bb6d9a7cfe6218a40",
    "createtime": "2023-04-06 07:42:26",
    "credentialimg": null,
    "entGasorderGoods": [ //商品信息
      {
        "id": "974598838f7746b287311699c6325de9",
        "orderid": "a0698ee656c14dc694884ad138e3b307",
        "goodid": "7985c6185c3f4b0687e61e3097fccc50",
        "goodimg": "/a/b/c.jpg",
        "goodname": "商品图片下面的那个又大又黑的主标题1",
        "goodtype": "液化气",
        "goodmodel": "商品型号4",
        "goodunitprice": 0,
        "goodnum": 2,
        "gooddesc": "主标题下面的那个灰灰的小小的副标题2",
        "goodsdetail": "{\"goodFy\":[{\"createTime\":\"2023-03-31 15:36:51\",\"creater\":\"test@001租户管理员\",\"ed\":\"2023-05-01\",\"goodsId\":\"7985c6185c3f4b0687e61e3097fccc50\",\"id\":\"12e1beb75e5843d292f19ae729b502a4\",\"lastUpdateTime\":\"2023-03-31 17:21:43\",\"lastUpdater\":\"test@001租户管理员\",\"sfbm\":\"cs11\",\"sfdw\":\"瓶\",\"sffx\":0,\"sfje\":9900,\"sfmc\":\"瓶装液化石油气\",\"sfms\":\"一瓶液化气的钱啊\",\"sfqt\":[\"factory\",\"restaurant\"],\"sortIndex\":0,\"st\":\"2023-03-01\",\"state\":\"1\"},{\"createTime\":\"2023-04-03 11:02:37\",\"creater\":\"test@001租户管理员\",\"ed\":\"2099-01-01\",\"goodsId\":\"7985c6185c3f4b0687e61e3097fccc50\",\"id\":\"6c5506fb301b45a3ab485d1d889bd5bb\",\"sfbm\":\"sl01\",\"sfdw\":\"次\",\"sffx\":0,\"sfje\":1000,\"sfmc\":\"上楼费\",\"sfms\":\"上楼费用\",\"sfqt\":[\"factory\",\"restaurant\"],\"sortIndex\":11,\"st\":\"2023-01-01\",\"state\":\"1\"}],\"good\":{\"coverPic\":{\"basename\":\"picTest\",\"fileUrl\":\"/a/b/c.jpg\",\"filetype\":\"jpeg\",\"fsize\":\"1024\",\"id\":\"7c34419b82e3d0b730fd82040c5\",\"status\":\"0\"},\"coverPicId\":\"7c34419b82e3d0b730fd82040c5\",\"createTime\":\"2023-03-31 10:51:12\",\"creater\":\"test@001租户管理员\",\"details\":\"超详细的商品介绍，来自富文本编辑器3\",\"ents\":[{\"areacode\":\"320206001\",\"areaname\":\"中华人民共和国,江苏省,无锡市,惠山区,堰桥街道\",\"qid\":\"9d14f4c79abb48a0a30fd82040c54640\",\"qname\":\"测试企业\",\"ssts\":[null,{\"gid\":\"9d14f4c79abb48a0a30fd82040c59012\",\"gname\":\"测试供应站\"}]}],\"goodsType\":{\"createTime\":\"2023-03-31 10:44:56\",\"creater\":\"test@001租户管理员\",\"goodsType\":\"液化气\",\"goodsTypeRemark\":\"瓶装液化石油气\",\"id\":\"611ce99f084148e186cccc8c263f3b8c\",\"parentId\":\"0\"},\"id\":\"7985c6185c3f4b0687e61e3097fccc50\",\"lastUpdateTime\":\"2023-03-31 10:59:37\",\"lastUpdater\":\"test@001租户管理员\",\"maintitle\":\"商品图片下面的那个又大又黑的主标题1\",\"model\":\"商品型号4\",\"remark\":\"商品备注5\",\"saleTypes\":[\"telphone\",\"wechat\"],\"saletimeEnd\":\"09:01\",\"saletimeStart\":\"17:59\",\"sortIndex\":1,\"state\":\"IN_SALE\",\"stateText\":\"销售中\",\"stock\":99,\"subtitle\":\"主标题下面的那个灰灰的小小的副标题2\",\"titlePics\":[{\"basename\":\"picTest\",\"fileUrl\":\"/a/b/c.jpg\",\"filetype\":\"jpeg\",\"fsize\":\"1024\",\"id\":\"7c34419b82e3d0b730fd82040c5\",\"status\":\"0\"}],\"typeId\":\"611ce99f084148e186cccc8c263f3b8c\"}}",
        "totalprice": 0,
        "createtime": "2023-04-06 07:42:26"
      }
    ],
    "flows": [// 流水信息
      {
        "id": "2d979dc0ba9944d697c3fb53aa0fdb7f",
        "orderid": "a0698ee656c14dc694884ad138e3b307",
        "transactionflowid": null,
        "operatetime": "2023-04-06 07:42:26",
        "operateman": "user_zMNImU",
        "operatemanid": "640a3a775c2e4b6bb6d9a7cfe6218a40",
        "operatetype": "create",
        "operatecontent": "新建订单",
        "linkedkey": null,
        "linkeddata": null
      }
    ]
  },
  "desc": "成功"
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 新建订单

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/createOrder
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderman": "下单人25",
  "ordermanid": "下单人标识34",
  "ordertel": "下单人联系电话86",
  "uinfoid": "用户标识89",
  "supplyent": "供气企业114",
  "supplyentid": "供气企业标识64",
  "supplystation": "所属门店23",
  "supplystationid": "所属门店标识57",
  "commodities": [
    {
      "commodityid": "商品标识56",
      "commoditynum": 477.37178410272566
    },
    {
      "commodityid": "商品标识56",
      "commoditynum": 477.37178410272566
    }
  ],
  "buychannel": "订购渠道104",
  "useintegralnum": 849.5571990840497,
  "discountprice": 80.36564827439739,
  "paymode": "支付方式22",
  "assignmanid": "派单人标识109",
  "appointmentarrivetime": "预约送达时间35",
  "remark": "备注114"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderman     | **String**     | 否  |  下单人 |   |
| ordermanid     | **String**     | 否  |  下单人标识 |   |
| ordertel     | **String**     | 否  |  下单人联系电话 |   |
| uinfoid     | **String**     | 否  |  用户标识 |   |
| supplyent     | **String**     | 否  |  供气企业 |   |
| supplyentid     | **String**     | 否  |  供气企业标识 |   |
| supplystation     | **String**     | 否  |  所属门店 |   |
| supplystationid     | **String**     | 否  |  所属门店标识 |   |
| commodities     | **List\<GasCommodityOrderInput\>**     | 否  |  购买商品信息 |   |
|└─ commodityid     | **String**     | 否  |  商品标识 |   |
|└─ commoditynum     | **BigDecimal**     | 否  |  商品数量 |   |
| buychannel     | **String**     | 否  |  订购渠道 |   |
| useintegralnum     | **BigDecimal**     | 否  |  使用积分数 |   |
| discountprice     | **BigDecimal**     | 否  |  优惠金额 |   |
| paymode     | **String**     | 否  |  支付方式 |   |
| assignmanid     | **String**     | 否  |  派单人标识 |   |
| appointmentarrivetime     | **String**     | 否  |  预约送达时间 |   |
| remark     | **String**     | 否  |  备注 |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 派单

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/assign
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "xVUcePRHkxLmhC",
  "workmanid": "URqYEWvt"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |
| workmanid     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 接单

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/receive
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "HKENuwE"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 订单送达

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/arrive
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "RhbYHoscpkx",
  "arrivelocation": "XbVIERJEEP",
  "signedbyimg": "签收人确认图片71",
  "signedstate": "签收状态2",
  "signedtime": "签收时间104",
  "credentialimg": "凭据图片10"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |
| arrivelocation     | **String**     | 否  |   |   |
| signedbyimg     | **String**     | 否  |  签收人确认图片 |   |
| signedstate     | **String**     | 否  |  签收状态 |   |
| signedtime     | **String**     | 否  |  签收时间 |   |
| credentialimg     | **String**     | 否  |  凭据图片 |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 确认收款

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/confirmReceipt
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "NLcyHxFyllwYffUk"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 退单

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/chargeBack
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "pvXdqBkSnYrtHO",
  "chargebackreason": "退单原因34"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |
| chargebackreason     | **String**     | 否  |  退单原因 |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
 [返回目录](#目录)
# 退单审核

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/chargeBackConfirme
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "mNUswKTifTPfhXDeJ",
  "chargebackconfirmremark": "退单审核备注58",
  "result": "审核结果65"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |
| chargebackconfirmremark     | **String**     | 否  |  退单审核备注 |   |
| result     | **String**     | 否  |  审核结果 |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 微信公众号支付

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/gasOrder/pay4WeChatOfficialAccount
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "orderid": "MFTvErfWoi",
  "openid": "RUhxBkul"
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| orderid     | **String**     | 否  |   |   |
| openid     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |




 [返回目录](#目录)
# 新增商品

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/save
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "lygBtkqhJXddpb",
  "typeId": "GgFOUGgAqWU",
  "goodsType": {
    "id": "KwLtMfguiSwOXGSRSr",
    "parentId": "ofcukHFKHb",
    "goodsType": "HLBPXEbJtfvdQtcd",
    "goodsTypeRemark": "WmKuIcA",
    "creater": "ukBryStgBXYKGkhXqr",
    "createTime": "kSvDRoDTtiR",
    "lastUpdater": "BMwdhnvTofSIKoNb",
    "lastUpdateTime": "LNBongqWGDolH",
    "ents": [
      {
        "qid": "vJqja",
        "qname": "FakCXLXVrJHvUDGRpUS",
        "areacode": "PDpqxkWoFQuBu",
        "areaname": "PsraH",
        "bm": "owDHOKOxw",
        "ssts": [
          {
            "gid": "nQmEKYKOTevAdfi",
            "gname": "LlumrAuYeGPTiI",
            "gaddress": "QtDRAiaydxxGwfpru",
            "areacode": "DhDFDOXUVBTrYBq",
            "areaname": "WcihyXKwYKxjyuHo"
          },
          {
            "gid": "nQmEKYKOTevAdfi",
            "gname": "LlumrAuYeGPTiI",
            "gaddress": "QtDRAiaydxxGwfpru",
            "areacode": "DhDFDOXUVBTrYBq",
            "areaname": "WcihyXKwYKxjyuHo"
          }
        ]
      },
      {
        "qid": "vJqja",
        "qname": "FakCXLXVrJHvUDGRpUS",
        "areacode": "PDpqxkWoFQuBu",
        "areaname": "PsraH",
        "bm": "owDHOKOxw",
        "ssts": [
          {
            "gid": "nQmEKYKOTevAdfi",
            "gname": "LlumrAuYeGPTiI",
            "gaddress": "QtDRAiaydxxGwfpru",
            "areacode": "DhDFDOXUVBTrYBq",
            "areaname": "WcihyXKwYKxjyuHo"
          },
          {
            "gid": "nQmEKYKOTevAdfi",
            "gname": "LlumrAuYeGPTiI",
            "gaddress": "QtDRAiaydxxGwfpru",
            "areacode": "DhDFDOXUVBTrYBq",
            "areaname": "WcihyXKwYKxjyuHo"
          }
        ]
      }
    ]
  },
  "coverPicId": "FydpDdEDX",
  "maintitle": "uiehablxcaySRRsUHE",
  "subtitle": "bQHcdrfAaEDufYoagb",
  "model": "GIFVaC",
  "state": "PUDFxHBvXFEqNsQ",
  "stateText": "YYvCOUGDdGpAi",
  "details": "sGrgwBbLOCSKg",
  "creater": "vUAFsAJJ",
  "createTime": "beLMMpy",
  "lastUpdater": "vOBgOnWCHQSytPomDQD",
  "lastUpdateTime": "FWTEQmSl",
  "remark": "jsJvYMOgqQxCAb",
  "saletimeStart": "oEwDSyofGcIAd",
  "saletimeEnd": "lNrUEPCwUFvPB",
  "sortIndex": 984,
  "coverPic": {
    "id": "vXNPO",
    "basename": "IHrRGbAeoSqXNhK",
    "fileUrl": "EAHYUTH",
    "fsize": "gJLKbe",
    "filetype": "JULlTRxhoWE",
    "status": "EUrNnFM"
  },
  "titlePics": [
    {
      "id": "oiKuxYwaCGBmFmejb",
      "basename": "fEwpUJfooHvKvSX",
      "fileUrl": "yHwNwwjkGlM",
      "fsize": "QNjNi",
      "filetype": "OvTPvqWIKGvRsoDFAiA",
      "status": "PMNsiJuBL"
    },
    {
      "id": "oiKuxYwaCGBmFmejb",
      "basename": "fEwpUJfooHvKvSX",
      "fileUrl": "yHwNwwjkGlM",
      "fsize": "QNjNi",
      "filetype": "OvTPvqWIKGvRsoDFAiA",
      "status": "PMNsiJuBL"
    }
  ],
  "ents": [
    {
      "qid": "UekFFfYIp",
      "qname": "wKmojv",
      "areacode": "TSdtVce",
      "areaname": "GXsvdiDlLfrmWrcsQ",
      "bm": "jgdQRPLtpAt",
      "ssts": [
        {
          "gid": "HrUkqlYaMefHkIdFNB",
          "gname": "jjoqewtT",
          "gaddress": "OKnEMchOs",
          "areacode": "RfXqHNlrVBWrqGqSQ",
          "areaname": "PEEFydP"
        },
        {
          "gid": "HrUkqlYaMefHkIdFNB",
          "gname": "jjoqewtT",
          "gaddress": "OKnEMchOs",
          "areacode": "RfXqHNlrVBWrqGqSQ",
          "areaname": "PEEFydP"
        }
      ]
    },
    {
      "qid": "UekFFfYIp",
      "qname": "wKmojv",
      "areacode": "TSdtVce",
      "areaname": "GXsvdiDlLfrmWrcsQ",
      "bm": "jgdQRPLtpAt",
      "ssts": [
        {
          "gid": "HrUkqlYaMefHkIdFNB",
          "gname": "jjoqewtT",
          "gaddress": "OKnEMchOs",
          "areacode": "RfXqHNlrVBWrqGqSQ",
          "areaname": "PEEFydP"
        },
        {
          "gid": "HrUkqlYaMefHkIdFNB",
          "gname": "jjoqewtT",
          "gaddress": "OKnEMchOs",
          "areacode": "RfXqHNlrVBWrqGqSQ",
          "areaname": "PEEFydP"
        }
      ]
    }
  ],
  "stock": 553,
  "saleTypes": [
    "AoiutlYCijnsQrTwKrc",
    "qvIbcfdfreoQol"
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| typeId     | **String**     | 否  |   |   |
| goodsType     | **GoodsType**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ parentId     | **String**     | 否  |   |   |
|└─ goodsType     | **String**     | 否  |   |   |
|└─ goodsTypeRemark     | **String**     | 否  |   |   |
|└─ creater     | **String**     | 否  |   |   |
|└─ createTime     | **String**     | 否  |   |   |
|└─ lastUpdater     | **String**     | 否  |   |   |
|└─ lastUpdateTime     | **String**     | 否  |   |   |
|└─ ents     | **List\<GasEnt\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ qid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ qname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ bm     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
| coverPicId     | **String**     | 否  |   |   |
| maintitle     | **String**     | 否  |   |   |
| subtitle     | **String**     | 否  |   |   |
| model     | **String**     | 否  |   |   |
| state     | **String**     | 否  |   |   |
| stateText     | **String**     | 否  |   |   |
| details     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| remark     | **String**     | 否  |   |   |
| saletimeStart     | **String**     | 否  |   |   |
| saletimeEnd     | **String**     | 否  |   |   |
| sortIndex     | **Integer**     | 否  |   |   |
| coverPic     | **GoodsFile**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ basename     | **String**     | 否  |   |   |
|└─ fileUrl     | **String**     | 否  |   |   |
|└─ fsize     | **String**     | 否  |   |   |
|└─ filetype     | **String**     | 否  |   |   |
|└─ status     | **String**     | 否  |   |   |
| titlePics     | **List\<GoodsFile\>**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ basename     | **String**     | 否  |   |   |
|└─ fileUrl     | **String**     | 否  |   |   |
|└─ fsize     | **String**     | 否  |   |   |
|└─ filetype     | **String**     | 否  |   |   |
|└─ status     | **String**     | 否  |   |   |
| ents     | **List\<GasEnt\>**     | 否  |   |   |
|└─ qid     | **String**     | 否  |   |   |
|└─ qname     | **String**     | 否  |   |   |
|└─ areacode     | **String**     | 否  |   |   |
|└─ areaname     | **String**     | 否  |   |   |
|└─ bm     | **String**     | 否  |   |   |
|└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
| stock     | **int**     | 否  |   |   |
| saleTypes     | **List\<String\>**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 删除商品

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/remove/{goodsId}
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
//goodsId:TcWhxlAgdxWSHCr

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品，只修改商品基础信息

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/update/text
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "FqBaRadyRlsxkoE",
  "typeId": "BASNbewY",
  "goodsType": {
    "id": "hmOUwKubqeiTUARL",
    "parentId": "MTNGYUdQRciua",
    "goodsType": "xflLB",
    "goodsTypeRemark": "hKfthCHdVqKdlRDlrw",
    "creater": "HDRsRtBmEV",
    "createTime": "edDthiHVLRR",
    "lastUpdater": "QpxhPVwwDketYFtFn",
    "lastUpdateTime": "GBhNLRxTCsedntPsRI",
    "ents": [
      {
        "qid": "MRLJjavLia",
        "qname": "IKYGHlKjGRqDHcG",
        "areacode": "EJHaAKHNFUxnf",
        "areaname": "dbljTQ",
        "bm": "lNqmnVHjxEMGclDOq",
        "ssts": [
          {
            "gid": "TLsgeCRxHXnljBI",
            "gname": "hRCqKuuy",
            "gaddress": "uYSJeue",
            "areacode": "ktptcBegI",
            "areaname": "tMlQerfnIaOCAbiV"
          },
          {
            "gid": "TLsgeCRxHXnljBI",
            "gname": "hRCqKuuy",
            "gaddress": "uYSJeue",
            "areacode": "ktptcBegI",
            "areaname": "tMlQerfnIaOCAbiV"
          }
        ]
      },
      {
        "qid": "MRLJjavLia",
        "qname": "IKYGHlKjGRqDHcG",
        "areacode": "EJHaAKHNFUxnf",
        "areaname": "dbljTQ",
        "bm": "lNqmnVHjxEMGclDOq",
        "ssts": [
          {
            "gid": "TLsgeCRxHXnljBI",
            "gname": "hRCqKuuy",
            "gaddress": "uYSJeue",
            "areacode": "ktptcBegI",
            "areaname": "tMlQerfnIaOCAbiV"
          },
          {
            "gid": "TLsgeCRxHXnljBI",
            "gname": "hRCqKuuy",
            "gaddress": "uYSJeue",
            "areacode": "ktptcBegI",
            "areaname": "tMlQerfnIaOCAbiV"
          }
        ]
      }
    ]
  },
  "coverPicId": "TLuOwGFotgTJwS",
  "maintitle": "mQlrWoRmToexOmsutIR",
  "subtitle": "NBeUpHPsjNtw",
  "model": "EdYGRKwGW",
  "state": "oAucEAwvRcAHe",
  "stateText": "MiWwhCjSeNVNGqJOHGk",
  "details": "qxOPiIxDR",
  "creater": "vfTKIXvQrcVcdIv",
  "createTime": "udokdP",
  "lastUpdater": "dORREqQXFTpkoPG",
  "lastUpdateTime": "HtBBNBcyesyHAkkxCC",
  "remark": "eDegwiTmcpVb",
  "saletimeStart": "uyXLvPWp",
  "saletimeEnd": "eQRPVevBWxuE",
  "sortIndex": 614,
  "coverPic": {
    "id": "ERMQJcVCXkqJSJAbSj",
    "basename": "tRPuXwihQaSdU",
    "fileUrl": "FRlCHxDwTlFddHqxOtm",
    "fsize": "WKLbuxiWtCUjEyscA",
    "filetype": "nMeyuSfndtqAKMf",
    "status": "ieLUQxyJor"
  },
  "titlePics": [
    {
      "id": "YQwshhOthrN",
      "basename": "AODJnroHGl",
      "fileUrl": "hUrRUNHBQ",
      "fsize": "TiygDgsj",
      "filetype": "bWexm",
      "status": "JCgmFghxPLOfkBieBB"
    },
    {
      "id": "YQwshhOthrN",
      "basename": "AODJnroHGl",
      "fileUrl": "hUrRUNHBQ",
      "fsize": "TiygDgsj",
      "filetype": "bWexm",
      "status": "JCgmFghxPLOfkBieBB"
    }
  ],
  "ents": [
    {
      "qid": "mpajHeNO",
      "qname": "ruMdu",
      "areacode": "VfDgaBBbRFrCFnBelKW",
      "areaname": "cOLKgUftqhe",
      "bm": "llkBcLEkiiP",
      "ssts": [
        {
          "gid": "INmoHQB",
          "gname": "XEVknSSjKxYp",
          "gaddress": "ammoaeXVdjfWGlTFERr",
          "areacode": "gXnbjxtpHkUHkwMOLK",
          "areaname": "XefGq"
        },
        {
          "gid": "INmoHQB",
          "gname": "XEVknSSjKxYp",
          "gaddress": "ammoaeXVdjfWGlTFERr",
          "areacode": "gXnbjxtpHkUHkwMOLK",
          "areaname": "XefGq"
        }
      ]
    },
    {
      "qid": "mpajHeNO",
      "qname": "ruMdu",
      "areacode": "VfDgaBBbRFrCFnBelKW",
      "areaname": "cOLKgUftqhe",
      "bm": "llkBcLEkiiP",
      "ssts": [
        {
          "gid": "INmoHQB",
          "gname": "XEVknSSjKxYp",
          "gaddress": "ammoaeXVdjfWGlTFERr",
          "areacode": "gXnbjxtpHkUHkwMOLK",
          "areaname": "XefGq"
        },
        {
          "gid": "INmoHQB",
          "gname": "XEVknSSjKxYp",
          "gaddress": "ammoaeXVdjfWGlTFERr",
          "areacode": "gXnbjxtpHkUHkwMOLK",
          "areaname": "XefGq"
        }
      ]
    }
  ],
  "stock": 996,
  "saleTypes": [
    "PDPWMKCNDNbrfJL",
    "KADFbF"
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| typeId     | **String**     | 否  |   |   |
| goodsType     | **GoodsType**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ parentId     | **String**     | 否  |   |   |
|└─ goodsType     | **String**     | 否  |   |   |
|└─ goodsTypeRemark     | **String**     | 否  |   |   |
|└─ creater     | **String**     | 否  |   |   |
|└─ createTime     | **String**     | 否  |   |   |
|└─ lastUpdater     | **String**     | 否  |   |   |
|└─ lastUpdateTime     | **String**     | 否  |   |   |
|└─ ents     | **List\<GasEnt\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ qid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ qname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ bm     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
| coverPicId     | **String**     | 否  |   |   |
| maintitle     | **String**     | 否  |   |   |
| subtitle     | **String**     | 否  |   |   |
| model     | **String**     | 否  |   |   |
| state     | **String**     | 否  |   |   |
| stateText     | **String**     | 否  |   |   |
| details     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| remark     | **String**     | 否  |   |   |
| saletimeStart     | **String**     | 否  |   |   |
| saletimeEnd     | **String**     | 否  |   |   |
| sortIndex     | **Integer**     | 否  |   |   |
| coverPic     | **GoodsFile**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ basename     | **String**     | 否  |   |   |
|└─ fileUrl     | **String**     | 否  |   |   |
|└─ fsize     | **String**     | 否  |   |   |
|└─ filetype     | **String**     | 否  |   |   |
|└─ status     | **String**     | 否  |   |   |
| titlePics     | **List\<GoodsFile\>**     | 否  |   |   |
|└─ id     | **String**     | 否  |   |   |
|└─ basename     | **String**     | 否  |   |   |
|└─ fileUrl     | **String**     | 否  |   |   |
|└─ fsize     | **String**     | 否  |   |   |
|└─ filetype     | **String**     | 否  |   |   |
|└─ status     | **String**     | 否  |   |   |
| ents     | **List\<GasEnt\>**     | 否  |   |   |
|└─ qid     | **String**     | 否  |   |   |
|└─ qname     | **String**     | 否  |   |   |
|└─ areacode     | **String**     | 否  |   |   |
|└─ areaname     | **String**     | 否  |   |   |
|└─ bm     | **String**     | 否  |   |   |
|└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |
| stock     | **int**     | 否  |   |   |
| saleTypes     | **List\<String\>**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品销售状态，待售，销售中，下架

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/update/state
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
goodsId:pJrtrfrQvFctm
state:DHNRhVVQpw

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | **是**  |   |   |
| state     | **String**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品库存

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/update/stock
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
goodsId:CvVyKbkhMHBuPx
stock:383

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | **是**  |   |   |
| stock     | **Integer**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品标题轮播图片

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/update/titlePic
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
goodsId:ykDpkoGUrlcKyLGvY
titlePics[][0]:nPiKsJVBqjEFJn
titlePics[][1]:RNYxyWSvWT

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | **是**  |   |   |
| titlePics[]     | **String[]**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品关联企业门店

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/update/ents
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "goodsId": "IlIJf",
  "ents": [
    {
      "qid": "QbeywRixKjvT",
      "qname": "tnfXudywrOrTpgBTj",
      "areacode": "NdBHJCyx",
      "areaname": "VFDrXjvsHEnDN",
      "bm": "DeKHfUP",
      "ssts": [
        {
          "gid": "bMRudyoPqqKGFty",
          "gname": "niAFpvGtE",
          "gaddress": "YPDdRnT",
          "areacode": "EkbUjuHXoGoKmrtxwr",
          "areaname": "nQWim"
        },
        {
          "gid": "bMRudyoPqqKGFty",
          "gname": "niAFpvGtE",
          "gaddress": "YPDdRnT",
          "areacode": "EkbUjuHXoGoKmrtxwr",
          "areaname": "nQWim"
        }
      ]
    },
    {
      "qid": "QbeywRixKjvT",
      "qname": "tnfXudywrOrTpgBTj",
      "areacode": "NdBHJCyx",
      "areaname": "VFDrXjvsHEnDN",
      "bm": "DeKHfUP",
      "ssts": [
        {
          "gid": "bMRudyoPqqKGFty",
          "gname": "niAFpvGtE",
          "gaddress": "YPDdRnT",
          "areacode": "EkbUjuHXoGoKmrtxwr",
          "areaname": "nQWim"
        },
        {
          "gid": "bMRudyoPqqKGFty",
          "gname": "niAFpvGtE",
          "gaddress": "YPDdRnT",
          "areacode": "EkbUjuHXoGoKmrtxwr",
          "areaname": "nQWim"
        }
      ]
    }
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | 否  |   |   |
| ents     | **List\<GasEnt\>**     | 否  |   |   |
|└─ qid     | **String**     | 否  |   |   |
|└─ qname     | **String**     | 否  |   |   |
|└─ areacode     | **String**     | 否  |   |   |
|└─ areaname     | **String**     | 否  |   |   |
|└─ bm     | **String**     | 否  |   |   |
|└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 无权限获取商品详细信息

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/public/get/{id}?id=pvwWbEEpLQmiyXd
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
//id:pvwWbEEpLQmiyXd

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 无权限获取商品列表

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/public/list?pageNumber=wmLYfMMyMVCXUk&pageSize=qvgYNVnRqc&tid=irNVMpJoQiwcjEFtO&qid=MnhJbvbp&gid=aVKoowlKPV&search=ljwReoWfFmyVlOn
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
pageNumber:wmLYfMMyMVCXUk
pageSize:qvgYNVnRqc
//tid:irNVMpJoQiwcjEFtO
//qid:MnhJbvbp
//gid:aVKoowlKPV
//search:ljwReoWfFmyVlOn

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNumber     | **String**     | **是**  |   |   |
| pageSize     | **String**     | **是**  |   |   |
| tid     | **String**     | 否  |   |   |
| qid     | **String**     | 否  |   |   |
| gid     | **String**     | 否  |   |   |
| search     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 获取所有商品信息

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/list?pageNumber=ILIXSoDfYGdLyJWcEQY&pageSize=IaOawQBXfkbNnbUIbrq&tid=nQHyOVIvmBNipVnY&qid=MCxcEFKebunQkjMSSw&gid=bpGrBsbP&search=hgsvakaUbrDAVmr
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
pageNumber:ILIXSoDfYGdLyJWcEQY
pageSize:IaOawQBXfkbNnbUIbrq
//tid:nQHyOVIvmBNipVnY
//qid:MCxcEFKebunQkjMSSw
//gid:bpGrBsbP
//search:hgsvakaUbrDAVmr

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNumber     | **String**     | **是**  |   |   |
| pageSize     | **String**     | **是**  |   |   |
| tid     | **String**     | 否  |   |   |
| qid     | **String**     | 否  |   |   |
| gid     | **String**     | 否  |   |   |
| search     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |




 [返回目录](#目录)
# 新增商品费用

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/save
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "eBhsegmabtmsLqlB",
  "goodsId": "SRAlRFhcmLLGp",
  "sfbm": "IVgKBKTPXXIgUhgUog",
  "sfmc": "XmmKUcTNmhEVIaaNNIv",
  "sfdw": "WpepAheblLAUNLbIy",
  "sfje": 486,
  "sfms": "aqRIlRULAmYxyh",
  "sfqtname": "qbwMquY",
  "creater": "jxLpFqVgrn",
  "createTime": "pQsrFD",
  "lastUpdater": "KofEphkHO",
  "lastUpdateTime": "FoHRDUGTgQkCotHk",
  "state": "dAFkuCr",
  "st": "nehyggFiOQOKmuElbXO",
  "ed": "WLYNDUVsFXfkltbW",
  "sffx": 700,
  "sortIndex": 55,
  "sfqt": [
    "eFaqTScTtRdGlmIh",
    "GKsYIIlK"
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| goodsId     | **String**     | 否  |   |   |
| sfbm     | **String**     | 否  |   |   |
| sfmc     | **String**     | 否  |   |   |
| sfdw     | **String**     | 否  |   |   |
| sfje     | **Integer**     | 否  |   |   |
| sfms     | **String**     | 否  |   |   |
| sfqtname     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| state     | **String**     | 否  |   |   |
| st     | **String**     | 否  |   |   |
| ed     | **String**     | 否  |   |   |
| sffx     | **Integer**     | 否  |   |   |
| sortIndex     | **Integer**     | 否  |   |   |
| sfqt     | **List\<String\>**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 删除商品费用

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/remove/{id}
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
//id:hfyeFTEYywRicGe

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品费用信息

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/update
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "XyxXnnMGjwjbh",
  "goodsId": "eDLjBPscBEE",
  "sfbm": "lAKVUDtNbrtfGWCDfRF",
  "sfmc": "IEpbmYuumLxQbK",
  "sfdw": "lxMKddjPT",
  "sfje": 560,
  "sfms": "GYJUJBRoT",
  "sfqtname": "eLgjXsxB",
  "creater": "fuccVpqiaL",
  "createTime": "SLWOFedENCl",
  "lastUpdater": "QXpJstFDlETIEjaRM",
  "lastUpdateTime": "uVXUbABMUXp",
  "state": "tfVxUX",
  "st": "LGglmuqcPAhhQebeJae",
  "ed": "nsPAbwl",
  "sffx": 848,
  "sortIndex": 905,
  "sfqt": [
    "dvRrLUdAlLQchkXBkl",
    "fcHXYfPVxDyvlQjcx"
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| goodsId     | **String**     | 否  |   |   |
| sfbm     | **String**     | 否  |   |   |
| sfmc     | **String**     | 否  |   |   |
| sfdw     | **String**     | 否  |   |   |
| sfje     | **Integer**     | 否  |   |   |
| sfms     | **String**     | 否  |   |   |
| sfqtname     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| state     | **String**     | 否  |   |   |
| st     | **String**     | 否  |   |   |
| ed     | **String**     | 否  |   |   |
| sffx     | **Integer**     | 否  |   |   |
| sortIndex     | **Integer**     | 否  |   |   |
| sfqt     | **List\<String\>**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品费用是否启用

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/updateState
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
id:xKnKqY
state:YiWJCOj

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | **是**  |   |   |
| state     | **String**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 无权限根据商品id获取商品费用

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/public/list?goodsId=DRRbbfmRK&qt=tWYOlWndD
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
goodsId:DRRbbfmRK
//qt:tWYOlWndD

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| goodsId     | **String**     | **是**  |   |   |
| qt     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 根据商品id获取商品费用

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/fy/list?pageNumber=yfWUQixnmLnsGunNd&pageSize=mGMTkaaO&goodsId=SJkehdTVwanwmhniAG
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
pageNumber:yfWUQixnmLnsGunNd
pageSize:mGMTkaaO
goodsId:SJkehdTVwanwmhniAG

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNumber     | **String**     | **是**  |   |   |
| pageSize     | **String**     | **是**  |   |   |
| goodsId     | **String**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |




 [返回目录](#目录)
# 获取商品操作记录列表

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/operate/list?pageNumber=EPArWRPos&pageSize=hFUaBdLPy&goodsId=YwGDpNWxjBHJlRKthie
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
pageNumber:EPArWRPos
pageSize:hFUaBdLPy
goodsId:YwGDpNWxjBHJlRKthie

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNumber     | **String**     | **是**  |   |   |
| pageSize     | **String**     | **是**  |   |   |
| goodsId     | **String**     | **是**  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |




 [返回目录](#目录)
# 新增商品分类

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/save
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "QSHuNA",
  "parentId": "iMiejusvaB",
  "goodsType": "irKXogKNBuFoStQYhhG",
  "goodsTypeRemark": "vMDPafYACPagncuLwo",
  "creater": "lnqSFpMaLSJ",
  "createTime": "nDtmvotkjMgIhnEH",
  "lastUpdater": "ChMEaGwWecA",
  "lastUpdateTime": "tqlppUs",
  "ents": [
    {
      "qid": "pJhxyQTIDUDL",
      "qname": "lihTmbUjyoQvjSe",
      "areacode": "qopTVdyxjOx",
      "areaname": "LkkVQUttthMOiiP",
      "bm": "kgeQJJuj",
      "ssts": [
        {
          "gid": "naVtsblaHuTrAtr",
          "gname": "uotXxhnmhuOgPi",
          "gaddress": "PFqYyvBMFHPvsBP",
          "areacode": "bnfWVGDVhMcbtFAa",
          "areaname": "BVXvtfqtfRfuynkus"
        },
        {
          "gid": "naVtsblaHuTrAtr",
          "gname": "uotXxhnmhuOgPi",
          "gaddress": "PFqYyvBMFHPvsBP",
          "areacode": "bnfWVGDVhMcbtFAa",
          "areaname": "BVXvtfqtfRfuynkus"
        }
      ]
    },
    {
      "qid": "pJhxyQTIDUDL",
      "qname": "lihTmbUjyoQvjSe",
      "areacode": "qopTVdyxjOx",
      "areaname": "LkkVQUttthMOiiP",
      "bm": "kgeQJJuj",
      "ssts": [
        {
          "gid": "naVtsblaHuTrAtr",
          "gname": "uotXxhnmhuOgPi",
          "gaddress": "PFqYyvBMFHPvsBP",
          "areacode": "bnfWVGDVhMcbtFAa",
          "areaname": "BVXvtfqtfRfuynkus"
        },
        {
          "gid": "naVtsblaHuTrAtr",
          "gname": "uotXxhnmhuOgPi",
          "gaddress": "PFqYyvBMFHPvsBP",
          "areacode": "bnfWVGDVhMcbtFAa",
          "areaname": "BVXvtfqtfRfuynkus"
        }
      ]
    }
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| parentId     | **String**     | 否  |   |   |
| goodsType     | **String**     | 否  |   |   |
| goodsTypeRemark     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| ents     | **List\<GasEnt\>**     | 否  |   |   |
|└─ qid     | **String**     | 否  |   |   |
|└─ qname     | **String**     | 否  |   |   |
|└─ areacode     | **String**     | 否  |   |   |
|└─ areaname     | **String**     | 否  |   |   |
|└─ bm     | **String**     | 否  |   |   |
|└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 删除商品分类

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/remove/{id}
```

### 请求方法
```
POST
```

### 请求体类型
```
application/x-www-form-urlencoded
```

## 入参
### 入参示例 (Postman Bulk Edit)
```json
//id:Pxxijt

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 修改商品分类，包括修改关联企业

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/update
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "id": "ryYnMKGQVlHFfTGgLjP",
  "parentId": "AqugQgwiDPcQRC",
  "goodsType": "wTfalGJMXO",
  "goodsTypeRemark": "PaSfMAPMNLEJoIXVnPa",
  "creater": "pkpPFfWFd",
  "createTime": "ONAHsCokocPvoX",
  "lastUpdater": "yslaKFWhEIDcG",
  "lastUpdateTime": "KpeGJmlAfKbnG",
  "ents": [
    {
      "qid": "YvmnLEk",
      "qname": "ofDUQsf",
      "areacode": "QfkECYDKPTxTknM",
      "areaname": "WncwlxGPXadolW",
      "bm": "FPGUfHOOUFW",
      "ssts": [
        {
          "gid": "PkoMfVspA",
          "gname": "ikGdTVTicfX",
          "gaddress": "NpeBDWEtgiikDJPGMs",
          "areacode": "JhtjNAUIenp",
          "areaname": "XwirrTATXHkRNtI"
        },
        {
          "gid": "PkoMfVspA",
          "gname": "ikGdTVTicfX",
          "gaddress": "NpeBDWEtgiikDJPGMs",
          "areacode": "JhtjNAUIenp",
          "areaname": "XwirrTATXHkRNtI"
        }
      ]
    },
    {
      "qid": "YvmnLEk",
      "qname": "ofDUQsf",
      "areacode": "QfkECYDKPTxTknM",
      "areaname": "WncwlxGPXadolW",
      "bm": "FPGUfHOOUFW",
      "ssts": [
        {
          "gid": "PkoMfVspA",
          "gname": "ikGdTVTicfX",
          "gaddress": "NpeBDWEtgiikDJPGMs",
          "areacode": "JhtjNAUIenp",
          "areaname": "XwirrTATXHkRNtI"
        },
        {
          "gid": "PkoMfVspA",
          "gname": "ikGdTVTicfX",
          "gaddress": "NpeBDWEtgiikDJPGMs",
          "areacode": "JhtjNAUIenp",
          "areaname": "XwirrTATXHkRNtI"
        }
      ]
    }
  ]
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |
| parentId     | **String**     | 否  |   |   |
| goodsType     | **String**     | 否  |   |   |
| goodsTypeRemark     | **String**     | 否  |   |   |
| creater     | **String**     | 否  |   |   |
| createTime     | **String**     | 否  |   |   |
| lastUpdater     | **String**     | 否  |   |   |
| lastUpdateTime     | **String**     | 否  |   |   |
| ents     | **List\<GasEnt\>**     | 否  |   |   |
|└─ qid     | **String**     | 否  |   |   |
|└─ qname     | **String**     | 否  |   |   |
|└─ areacode     | **String**     | 否  |   |   |
|└─ areaname     | **String**     | 否  |   |   |
|└─ bm     | **String**     | 否  |   |   |
|└─ ssts     | **List\<GasSst\>**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gid     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gname     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ gaddress     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areacode     | **String**     | 否  |   |   |
|&ensp;&ensp;&ensp;&ensp;└─ areaname     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 根据id获取商品分类信息

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/get/{id}?id=BNPwqGJLlnqx
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
//id:BNPwqGJLlnqx

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| id     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 获取商品分类列表

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/list?pageNumber=HtVsMpPVhKqbqW&pageSize=KgqBfTqpnxSFYWtgD&type=uuiBiUuatLCKgUuEN&entId=yrvYmNkIbyAOXY&storeId=SVHptBxgiEWlDVV
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
pageNumber:HtVsMpPVhKqbqW
pageSize:KgqBfTqpnxSFYWtgD
//type:uuiBiUuatLCKgUuEN
//entId:yrvYmNkIbyAOXY
//storeId:SVHptBxgiEWlDVV

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| pageNumber     | **String**     | **是**  |   |   |
| pageSize     | **String**     | **是**  |   |   |
| type     | **String**     | 否  |   |   |
| entId     | **String**     | 否  |   |   |
| storeId     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |



 [返回目录](#目录)
# 公开获取商品分类列表

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/goods/type/public/list?type=XqNETcaqCQBVca&entId=OunKJspHnQfHLTGMg&storeId=xrgsEfIYNsDCFwUJNG
```

### 请求方法
```
GET
```


## 入参
### 入参示例 (Postman Bulk Edit)
```json
//type:XqNETcaqCQBVca
//entId:OunKJspHnQfHLTGMg
//storeId:xrgsEfIYNsDCFwUJNG

```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| type     | **String**     | 否  |   |   |
| entId     | **String**     | 否  |   |   |
| storeId     | **String**     | 否  |   |   |

## 出参
### 出参示例
```json
{
  "key1": {},
  "key2": {}
}
```


### 出参字段说明

| **字段** | **类型**  | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- |
| root     | **Map\<String, Object\>**    |   |   |




 [返回目录](#目录)
# 付款回调

## 请求信息

### 请求地址
```
http://172.17.160.1:8080/weChatCallback/payNotice?msg={}
```

### 请求方法
```
POST
```

### 请求体类型
```
application/json
```

## 入参
### 入参示例 (RequestBody)
```json
{
  "msg": {}
}
```


### 入参字段说明

| **字段** | **类型** | **必填** | **含义** | **其他参考信息** |
| -------- | -------- | -------- | -------- | -------- |
| msg     | **JSONObject**     | 否  |   |   |

## 出参
> 此接口无任何出参





